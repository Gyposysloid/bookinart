function preloader() {
	var $preloader = $('.preloader');
    	$preloader.fadeOut('slow');
}

function hideme() {
	$('.hideme, .hideme-list2:not(:nth-of-type(-n+2)), .hideme-list3:not(:nth-of-type(-n+3))').each( function(i){
	    var top_of_object = $(this).offset().top;
	    var bottom_of_window = $(window).scrollTop() + $(window).height()*0.9;
	    if( bottom_of_window > top_of_object){    
	        $(this).animate({'opacity':'1'},500);      
	    }
	}); 
}

function fixedMenu() {
	if($(window).scrollTop()>=$('header').outerHeight()) {
		$('nav').css({
			'position' : 'fixed',
			'top' : '0'
		});
	} else if($(window).scrollTop()<=$('header').height()) {
			$('nav').css({
			'position' : 'absolute',
			'top' : '160px'
		});
	}
}

function bigSlider() {
	var masImg = [];
	$('.big-slider .bxslider li').each(function(i) {
		var bgImg = $(this).find('img')
		.css('opacity','0')
		.attr('src');
		$(this).css({'background' : 'url(' + bgImg + ') 60% 50% / cover'});
	});
}

function calcSumm() {
	var summa=0 ;
	var items = $('.wrap-item-cart').length;
	for(var i=1; i<= items; i++) {
		var itemSum =  parseInt($('.wrap-counting .counting:nth-child(' + i +') .counting-result').attr('data-singlecost')*$('.wrap-counting .counting:nth-child(' + i +') .num').text());
		summa = parseInt(summa) + itemSum;
	}	
	$('.counting-total').html(summa + ' <span class="rub">P</span>');
}

function arrow(){
	var items = $('.wrap-item-cart').length;


	calcSumm();		

	$('.counting .delete').on('click' , function(){
		summa = 0;
		$(this).parents('.wrap-item-cart').remove();
		var items = $('.wrap-item-cart').length;
		calcSumm();
		if(items === 0) {
			$('.counting-total').text('0 P');
		}
	});

	$('.counting .arrow').on('click', function() {
		items = $('.wrap-item-cart').length;
		var quantity = $(this).parents('.counting').find('.num-quantity');
		var cost = $(this).parents('.counting').find('.counting-result').attr('data-singlecost');	


		if($(this).hasClass('prev')) {
			var minV = quantity.attr('data-minvalue');

			if($(this).parents().hasClass('konstruktor-book')) {
				var count = (parseInt(quantity.val()) - 1);
				count = count < minV ? minV : count;
				quantity.val(count);
				quantity.parents('.counting').find('.counting-result').html(cost*parseInt(quantity.val()) + ' <span class="rub">P</span>');
				quantity.val(count);
			}
			else {
				var count = (parseInt(quantity.text()) - 1);
				count = count < minV ? minV : count;
				quantity.text(parseInt(count));
				quantity.parents('.counting').find('.counting-result').html(cost*parseInt(quantity.text()) + ' <span class="rub">P</span>');
				quantity.text(count);
			}
		} else {
			var count = (parseInt(quantity.val()));
			if($(this).parents().hasClass('konstruktor-book')) {
				quantity.val(count + 1); 
				quantity.parents('.counting').find('.counting-result').html(cost*parseInt(quantity.val()) + ' <span class="rub">P</span>');
			}
			else {
				quantity.text(parseInt(quantity.text()) + 1); 
				quantity.parents('.counting').find('.counting-result').html(cost*parseInt(quantity.text()) + ' <span class="rub">P</span>');
			}
		}

		//$(this).parent().find('.val-print-price').val($(this).parent().find('.num-quantity').text());

		calcSumm();	
	});
	return false;
};

function activity() {
	$('.radiochoose > *').click(function(){
		if ($(this).hasClass('i-user-size')) {
			$(this).removeClass('active').parent().find('.user-size').addClass('active');
		} else {
			$(this).parent().find('.active').removeClass('active');
			$(this).addClass('active');
			$(this).parent().find('input').removeAttr('checked');
			$(this).find('input').attr('checked','checked');
		}
		return false;
	});

	$('.wrap-block__delivery-names .curier').on('click', function() {
		$('.wrap-block__delivery-names .curier').removeClass('active');
		$(this).addClass('active');
	});
}

function bookMatelial() {
	$('.material-type span').click(function(){
		$(this).parents('table').find('.val-mat-back').attr({'data-price': '0', 'value' : '-1'});
		$(this).parents('table').find('.val-mat-front').attr({'data-price': '0', 'value' : '-1'});
		if($(this).hasClass('front')) {
			$(this).parents('table').find('.back').addClass('hidden');
		}
		else {
			$(this).parents('table').find('.back').removeClass('hidden');
		}
	});
}

function clientsReviews() {
	$('.wrap-clients .item a').on('click', function(){
		$('.wrap-clients .item a').removeClass('active');
		$(this).addClass('active');
		var name_box = $(this).attr('class').split(' ')[0];
		$('.reviews').addClass('hidden');
		$('.'+name_box).removeClass('hidden');
		return false;
	});
}

function coloredIt() {
	var coloredBG;
	$('.colored').on('click', function(){
		if ($(this).hasClass('none')) {
			$('.colored').css({'background-color':'', 'color':''});
		}
		if($(this).hasClass('single-colored')) {
			$('.colored.single-colored').not($(this)).css({'background-color':'', 'color':''});
		}
		coloredBG = $(this);
		$('.box-modal .farz').on('click', function(){
			$('.box-modal img').removeClass('active');
			$(this).find('img').addClass('active');
			color = $(this).find('img').attr('data-color');
			if (color === '#ededed') {
				coloredBG.css('color','#888');
			}
			else if (color === '#9d776e') {
				coloredBG.css('color','#fff');
			}
			coloredBG.css('background-color',color);
		});
	});
}

function personalization() {
	if ($('.personalization-to a').hasClass('active')) {
		var iprice = $('.personalization-to a.active').attr('data-price');
		var itarget = $('.personalization-to a.active').attr('data-target');
		$('.personalization-to a.active').parents('.personalization-to').find('.val-print-price').val(itarget).attr('data-price', iprice);
	}

	$('.personalization span').on('click', function() {
		$(this).parents('td').find('.help-block').remove();
		$('.personalization-to a').removeClass('active');
		if(!$(this).hasClass('none-plate')) {
			$('.personalization-to').addClass('price-field');
		} else {
			$('.personalization-to').removeClass('price-field');
			$('.personalization-to .val-print-price').val('-1').attr('data-price','0');
		}
	});

	$('.personalization-to a').on('click', function() {
		var iprice = $(this).attr('data-price');
		var itarget = $(this).attr('data-target');
		$(this).parents('.personalization-to').find('.val-print-price').val(itarget).attr('data-price', iprice);
	});


	$('.material-type > *').on('click', function(){
	 	var per = $(this).attr('data-personal');

	 	if($(this).hasClass('none-plate')) {
	 		$(this).parents('table').find('.plate').addClass('invisible');
	 		$(this).parents('table').find('.plate .val-pi, .material-type .val-pi, .material-type .val-mi').val('-1').attr('data-price','0');
	 		$(this).parents('table').find('.colored').attr('style','');
	 	}
	 	else {
	 		$(this).parents('table').find('.plate').removeClass('invisible');
	 	}

	 	if (per === 'sq-stamp') {
	 		$(this).parents('table').find('.name.plate').text('Размещение тиснения:');
	 	} else {
	 		if (per === 'sq-plate') {
	 			$(this).parents('table').find('.name.plate').text('Размещение пластины:');
	 		}
	 		else {
	 			if (per === 'sq-photo' || per === 'sq-photocorob'){
	 				$(this).parents('table').find('.name.plate').text('Размещение фотовставки:');
	 			}
	 		}
	 	}

	 	$(this).parents('table').find('.personalBlock > div[class*="sq-"]').addClass('hidden').removeClass('fill');
	 	$(this).parents('table').find('.personalBlock > div[class*="sq-"] a').removeClass('active');
	 	$(this).parents('table').find('.personalBlock > .'+ per).removeClass('hidden').addClass('fill');
	 	var aiprice = $(this).parents('table').find('.personalBlock > .'+ per + ' a:first-child').attr('data-price'); 
	 	var aitarget =  $(this).parents('table').find('.personalBlock > .'+ per + ' a:first-child').attr('data-target');
	 	$(this).parents('table').find('.personalBlock > .'+ per + ' a:first-child').addClass('active');
	 	$(this).parents('table').find('.personalBlock .val-print-price').val(aitarget).attr('data-price', aiprice);
	 });
}

/*turn-customChoose размещен в последней tr формы*/
function modalDesignTurns() {
	var activeItem, bgUsual = []; 
	$('.block-designTurns .item-collection .col-img img').each(function(i) {
		bgUsual[i] = $(this).attr('src');
	});

	$('.block-designTurns .item-collection').on('click', function() {
		$('.block-designTurns .item-collection').each(function(i) {
			$(this).find('img').attr('src', bgUsual[i]);
		});
		activeItem = $(this);
		$(this).parents('.block-designTurns').find('.item-collection').removeClass('active');
		var target = $(this).addClass('active').attr('data-target');
		$(this).parents('html').find('.cdt-4').val(target);
		$('.dt-inputs').find('.help-block').remove();
		$(this).attr('src', bgUsual);
		$(this).parents('html').find('.turn-customChoose').attr({'value':'-1', 'data-price' :'0'});
		$('.add-info_design span').html('0 <span class="rub">P</span>');
	});

	$('.dt-modal .farz').on('click', function() {
		$('.dt-modal .farz').removeClass('active');
		$(this).addClass('active');
		activeItem.find('img').attr('src', $(this).find('img').attr('src'));
		$(this).parents('html').find('.turn-customChoose').val($(this).find('img').attr('data-img')).attr('data-price', $(this).find('img').attr('data-price'));
		$('.add-info_design span').html($(this).find('img').attr('data-price') + ' <span class="rub">P</span>');
		return false;
	});
	return false;
}

function nextConstructorPage() {
	$('.box-konstruktr .items-table:not(:nth-of-type(1))').css('display','none');
	var i=1;

	$('td.order .prev-table').addClass('hidden');
	if (i!=$('.box-konstruktr .items-table').length) {
		$('td.order input').addClass('hidden');
	} else {
		$('td.order .next-table').addClass('hidden');
	}

	$('.next-table').on('click',function(){
		i++;
		$('.box-konstruktr .items-table').fadeOut(0);
		$('.box-konstruktr .items-table:nth-of-type(' + i +')').fadeIn(300);
		$('.box-konstruktr .price-table').fadeOut(0).fadeIn(300);
		if(i===$('.box-konstruktr .items-table').length) {
			$('td.order input').removeClass('hidden');
			$('td.order .next-table').addClass('hidden');
		} else {
			$('td.order input').addClass('hidden');
		}
		if(i>=2) {
			$('td.order .prev-table').removeClass('hidden');
		}
		return false;
	});

	$('.prev-table').on('click',function(){
		i--;
		$('td.order .next-table').removeClass('hidden');
		$('.box-konstruktr .items-table').fadeOut(0);
		$('.box-konstruktr .items-table:nth-of-type(' + i +')').fadeIn(300);
		$('.box-konstruktr .price-table').fadeOut(0).fadeIn(300);
		if(i<2) {
			$('td.order .prev-table').addClass('hidden');
		}
		if(i!=$('.box-konstruktr .items-table').length) {
			$('td.order input').addClass('hidden');
		}
		return false;
	});

	//валидация для формы
	$('.konstruktor input[type="submit"]').on('click', function(e) {
		if ($('.konstruktor .file-link.fill input').val() != "") {
			$('.dt-inputs').removeClass('price-field, fill').find('.help-block').remove();
		}

		var empty = [];
		var j=0;
		for (var k=0; k < $('.fill').length; k++) {
			if($('.fill').eq(k).parent().hasClass('plate') && !$('.fill').eq(k).children().hasClass('active')) {
				e.preventDefault();
				$('.fill').eq(k).append('<p class="help-block">Выберите размещение</p>');
				empty[j] = $('.fill').eq(k);
				j++;
			} 
			else if ($('.fill').eq(k).parent().hasClass('modalInput') && $('.fill').eq(k).attr('style') === undefined || $('.fill').eq(k).attr('style') === '' && !$('.fill').eq(k).hasClass('colored')) {
				e.preventDefault();
				$('.fill').eq(k).parents('td').find('.help-block').remove();
				if ($('.fill').eq(k).hasClass('fjs--forzac')) {
					$('.fill').eq(k).parents('td').append('<p class="help-block">Выберите вид форзаца</p>');
				}
				else {
					$('.fill').eq(k).parents('td').append('<p class="help-block">Выберите вид материала</p>');
				}
				empty[j] = $('.fill').eq(k);
				j++;
			} 
			else if ($('.fill').eq(k).find('.valid-url').val() === '') {
				e.preventDefault();
				$('.fill').eq(k).find('.help-block').remove();
				$('.fill').eq(k).append('<p class="help-block">Введите URL адрес</p>');
				empty[j] = $('.fill').eq(k);
				j++;
			} 
			else if ($('.fill').eq(k).children().hasClass('help-block')) {
				e.preventDefault();
				empty[j] = $('.fill').eq(k);
				j++;
			} 
			else if($('.fill').eq(k).hasClass('dt-inputs') && $('.fill.dt-inputs .cdt-4').val() === '-1') {
				e.preventDefault();
				$('.fill').eq(k).find('.help-block').remove();
				$('.fill').eq(k).append('<a href="#design-turns" class="help-block">выберите тип дизайн разворотов</a>');
				empty[j] = $('.fill').eq(k);
				j++;
			} 
			else if($('.fill').eq(k).hasClass('dt-inputs') && $('.turn-customChoose').val() === '-1') {
				e.preventDefault();
				$('.fill').eq(k).find('.help-block').remove();
				$('.fill').eq(k).append('<a href="#design-turns" class="help-block">Выберите вид дизайна разворотов</a>');
				empty[j] = $('.fill').eq(k);
				j++;
			} 
			else if (($('.fill').eq(k).hasClass('single-colored') && $('.fill').eq(k).hasClass('active')) && ($('.fill').eq(k).attr('style') === undefined || $('.fill').eq(k).attr('style') === '')) {
				e.preventDefault();
				$('.fill').eq(k).parent().find('.help-block').remove();
				$('.fill').eq(k).parent().append('<p class="help-block">Выберите тип персонализации</p>');
				empty[j] = $('.fill').eq(k);
				j++;
			}
		}
		if(empty.length != 0) {
			$('.items-table').fadeOut(0);
		}

		empty[0].parents('table').fadeIn(300);

		if(empty[0].parents('table').index() != $('.items-table').length-1 && empty[0].parents('table').index() != 0) {
			$('td.order input').addClass('hidden');
			$('td .next-table').removeClass('hidden');
			$('.price-table').fadeIn(300);
		} else if (empty[0].parents('table').index() === 0 && $('.konstruktor-book .items-table').length != 1) {
			$('td .prev-table, td.order input').addClass('hidden');
			$('td .next-table').removeClass('hidden');
			$('.price-table').fadeIn(300);
		}
		else {
			$('.price-table').fadeIn(300);
		}
		
		i = empty[0].parents('table').index()+1;
	});

	return false;
}

function valide() {
	var regex;
	$('.valid-url').on('keyup',function(){
		var validInput=$(this);
		if(validInput.val() != '') {
			regex =/^(?:([a-z]+):(?:([a-z]*):)?\/\/)?(?:([^:@]*)(?::([^:@]*))?@)?((?:[a-z0-9_-]+\.)+[a-z]{2,}|localhost|(?:(?:[01]?\d\d?|2[0-4]\d|25[0-5])\.){3}(?:(?:[01]?\d\d?|2[0-4]\d|25[0-5])))(?::(\d+))?(?:([^:\?\#]+))?(?:\?([^\#]+))?(?:\#([^\s]+))?$/i;
				if(!regex.test($(this).val())){
					validInput.parent('.file-link').find('.help-block').remove();
					validInput.parent('.file-link').append('<p class="help-block">Введите корректный адрес</p>');
				} else {
					validInput.parent('.file-link').find('.help-block').remove();
				}
		}
	});

	$('.valid-url').focus(function() {
		$(this).parents('td').find('.help-block').remove();
	});
}

function someParallax() {
	var marg= parseInt($(window).scrollTop()/2);
	$('.parallax-box img').css('margin-top',-marg/3 + 'px');
	$('.parallax-box .box-text').css({'opacity' : 100/marg, 'transform': 'translateY('+  marg/10+ 'px)'});
	if($(window).scrollTop() <=0) {
		$('.parallax-box .box-text').css({'opacity' : '1', 'transform' : 'translateY(0)'});
	}
}

function animateKatalog() {
	$('.splitter a').on('click', function() {
		var it = $(this).attr('data-value');
		var lintems = $('.katalog ul .'+it).length;

		if (it==='all') {
			$('.katalog ul li').fadeIn(800)
		} else {
			$('.katalog ul li').hide();

			$('.katalog ul .'+it).eq(i).fadeIn(800);
		}
	});
}

function konstructorSlider() {
	var showItem = 3;
	var itemsWidth = $('.konstruktor .bx-pager a').outerWidth();
	var allItem = $('.slider-book .bx-pager a').length;
	
	if (allItem <= showItem) {
		$('.slider-book .bx-controls-direction').hide();
	}

	$('.slider-book .bx-next').on('click', function() {
		var itemActive = parseInt($('.bx-pager a.active').attr('data-slide-index'));
		if(itemActive > showItem - 1 && itemActive != $('.bx-pager a').length) {
			var offsetTr = (allItem - (allItem - itemActive)) * itemsWidth;
			$('.bx-pager .container').css('transform', 'translateX(-' + offsetTr + 'px)');
		} else {
			$('.bx-pager .container').css('transform', 'translateX(0px)');
		}
	});	

	$('.slider-book .bx-prev').on('click', function() {
		var itemActive = parseInt($('.bx-pager a.active').attr('data-slide-index'));
		if(itemActive = parseInt($('.bx-pager a.active').attr('data-slide-index')) === $('.bx-pager a').length-1) {
			var offsetTr = ($('.bx-pager a').length - showItem) * itemsWidth;
			$('.bx-pager .container').css('transform', 'translateX(-' + offsetTr + 'px)');
		} else if(parseInt($('.bx-pager a.active').attr('data-slide-index')) < ($('.bx-pager a').length - showItem)) {
			$('.bx-pager .container').css('transform', 'translateX(0px)');
		}
	});	
}

function delivery() {
	if($('#type-input-1').attr('checked') === 'checked') {
		$('.field-setorderform-city, .field-setorderform-address').find('.help-block').text('');
		$('.field-setorderform-city, .field-setorderform-address').addClass('no-width');
		$('.field-setorderform-city, .field-setorderform-address').find('.help-block').text();
		$('field-setorderform-city input, field-setorderform-address input').val('');
	} else {
		$('.field-setorderform-city, .field-setorderform-address').removeClass('no-width');
	}

	if($('#type-input-2').attr('checked') === 'checked') {
		$('.del-company').removeClass('hidden');
		$('.picked-item').addClass('disabled');
		$('.del-company .picked-item').removeClass('disabled');
	} else {
		$('.del-company .picked-item').addClass('disabled');
		$('.del-company .picked-item').removeClass('active');
		$('.del-company').addClass('hidden');
	}

	if($('#type-input-3').attr('checked') === 'checked') {
		$('.transport-delivery ').removeClass('hidden');
		$('.picked-item').addClass('disabled');
		$('.transport-delivery .picked-item').removeClass('disabled');
	} else {
		$('.transport-delivery .picked-item').addClass('disabled');
		$('.transport-delivery .picked-item').removeClass('active');
		$('.transport-delivery ').addClass('hidden');
	}
}

function comboMatirial() {
	$('.close-btn, .modal-wrapper').on('click', function() {
		$('.modal-wrapper').fadeOut(300);
	});

	$('.take-print.modalInput, .btn-link.kamod-close').on('click', function() {
		if($(this).hasClass('combomat')) {
			$('.leatheretteModal, .leatherModal').find('.modals-back').remove();
			$('.leatheretteModal, .leatherModal').append('<span class="combomat back-turns btn btn-link kamod-close modals-back">Назад</span>');
			
			$('.combomat').on('click', function() {
				$('.combomatModal').parent().fadeIn(300);
				$(document).mouseup(function (e) {
				    var container = $(".box-modal");
				    if (container.has(e.target).length === 0){
				        container.parents('.modal-wrapper').fadeOut(300);
				    }
				});
			});
		} else {
			$('.leatheretteModal .owl-wrapper-outer, .leatherModal .owl-wrapper-outer').find('.clear.text-left').remove();
		}
	});

	$('.combomat').on('click', function() {
		$('.combomatModal').parent().fadeIn(300);
		$(document).mouseup(function (e) {
		    var container = $(".box-modal");
		    if (container.has(e.target).length === 0){
		        container.parents('.modal-wrapper').fadeOut(300);
		    }
		});
	});

	$('.box-modal_close').on('click', function() {
		$(this).parents('.modal-wrapper').fadeOut(300);
	});
}

function materialCorob() {
	$('.corobmat span').on('click', function() {
		var type = $(this).attr('data-target');
		$(this).parents('table').find('.material > div').addClass('hidden');
		$(this).parents('table').find('.material > div .take').attr('style','');
		$(this).parents('table').find('.material .val-mi').attr('data-price','0');
		$(this).parents('table').find('.material > div .take').removeClass('fill');
		$(this).parents('table').find('.material').find('.'+type).parent().removeClass('hidden');
		$(this).parents('table').find('.material').find('.'+type).find('.take').addClass('fill');
	});
}

function modalInput() {
	var itemClick;
	$('.forzac').parent().addClass('disabled');

	$('.none-bg').click(function() {
		itemClick.parent().find('.take-print').css('background-image','');
	});

	$('.modalInput').on('click', function() {
		itemClick = $(this);
		if(itemClick.hasClass('modalInput-none')) {
			var priceItem = $(this).attr('data-price');
			$(itemClick).parents('td').find('.val-pi').attr('value','');
			$(itemClick).parents('td').find('.val-print-price').attr('data-price', priceItem);
		} else {
			$('.box-modal:not(.dt-modal) .farz').on('click',function(e) {
				e.preventDefault();
				var attrShow = $(this).children('img').data('show'); 
	    		var attrImg = $(this).children('img').data('img'); 
	    		var priceImg = $(this).children('img').data('price');
	    		var srcImg =  $(this).children('img').attr('src');
	    		$('.box-modal:not(.dt-modal) .farz').removeClass('active');
	    		$(this).addClass('active');
				$(itemClick).parents('td').find('.val-mi').val(attrImg);
				$(itemClick).parents('td').find('.val-mi').attr('data-price',priceImg);
				$(itemClick).find('.take').css('background-image','url('+srcImg+')');
				if ( itemClick.hasClass('choose-forzacs')) {
					if (attrShow === true) {
						$('.forzac').parent().removeClass('disabled');
					} else {
						$('.forzac').parent().addClass('disabled');
						$('.forzac input').attr({'value':'-1', 'data-price':'0'});
						$('.forzac .take').css('background-image','');
					}
				}
				if (itemClick.parents().hasClass('material')) {
					if ($(itemClick).parents('div').hasClass('front')) {
						$(itemClick).parents('td').find('.val-mat-front').attr({'data-price': $(this).children('img').data('price'), 'value' : $(this).children('img').data('img')});
					} else {
						$(itemClick).parents('td').find('.val-mat-back').attr({'data-price': $(this).children('img').data('price'), 'value' : $(this).children('img').data('img')});
					}
				}
			});
		}
	});
}

function parenquantity() {
	$('.parentInput').on('click', function() {
		var itemClick = $(this);
	    var target = itemClick.data('target'); 
	    var price = itemClick.data('price'); 
	    if ($(this).parents('tr').find('td .modalInput').length > 1 && !$(this).hasClass('modalInput-none')) {
			$(this).parents('table').find('td .val-mi').val('-1');
			$(this).parents('table').find('td .val-mi').attr('data-price','0');
		}

		if(itemClick.hasClass('modalInput-none')) {
			$(itemClick).parents('td').find('.val-mi').val('-1');
		}

	    $(itemClick).parents('td').find('.val-mi').val('-1');
		$(itemClick).parents('.parentBlock').find('.val-pi').val(target);

		if ($(this).parents('.parentBlock').find('.parenquantity').length > 1) {
			$(this).parents('table').find('.personalBlock .parentBlock .val-pi').val('-1').attr('data-price', '0');
			$(this).parents('table').find('.material .val-mi').val('-1').attr('data-price','0');
			$(itemClick).parents('.parentBlock').find('.val-pi').val(target).attr('data-price',price);


		}
	});
}

function dvdDisable() {
	$('.book-size span').on('click', function() {
		if($(this).find('input').attr('data-disable-notch') === 'true') {
			$('.check-point, .dvd input').attr('checked', false);
			$('.dvd').removeClass('price-field');
			$('.dvd').parent().addClass('disabled');
			$('.fc-points').removeClass('price-field').css({'opacity':'0.5','pointer-events':'none'});
			$('.check-point').css('pointer-events','none');
			$('.fc-points').find('.price-field').removeClass('price-field').addClass('price-field-n');
			$('.fc-points').find('.help-block').remove();
		}
		else {
			$('.check-point, .dvd input:nth-child(1)').attr('checked', true);
			$('.dvd').addClass('price-field');
			$('.dvd').parent().removeClass('disabled');
			$('.fc-points').addClass('price-field').css({'opacity':'1','pointer-events':'all'});
			$('.check-point').css('pointer-events','all');
			$('.fc-points').find('.price-field-n').removeClass('price-field-n').addClass('price-field');
		}
	});
}

/*  .check-point must be added to input. 
.fc-points need add to items which you wanna connect with checked input. 
Add it near with class "price-field"   */
function checkAddAttr() {
	if($('.check-point').attr('checked') === 'checked') {
		$('.fc-points').addClass('price-field').css({'opacity':'1','pointer-events':'all'});
		$('.fc-points').find('.price-field-n').removeClass('price-field-n').addClass('price-field');
		$('.fc-points').find('.fill-n').removeClass('fill-n').addClass('fill');

	} else {
		$('.fc-points').removeClass('price-field').css({'opacity':'0.5','pointer-events':'none'});
		$('.fc-points').find('.price-field').removeClass('price-field').addClass('price-field-n');
		$('.fc-points').find('.help-block').remove();
		$('.fc-points').find('.fill').removeClass('fill').addClass('fill-n');
	}
}

/*
	price-field добавляется к тем td с которые необходимо пройти циклу 
	val-print-price добавляется к input с которых, или в которые следует списать/записать data-pice

	после добавления price-field необходимо указать метод счёта
	count-pager - считает кол-во из val * цену
	count-active - выберает цену из активного элемента
	count-checked- выберает цену из чекнутого элемента
	count-input - выберает цену из input с классом val-print-price
	count-personal - выберает цену из активного элемента (не нуждается в val-print-price) 
*/
function CalculateIt() {
	var masPrice = [], 
		masPriceMin = [],
		sum = 0,
		sumMin = 0,
		j = 0,
		countPrice = 0,
		base = parseInt($('.box-konstruktr').attr('data-price')),
		discount = parseInt($('.box-konstruktr').attr('data-sale'))/100,
		numberOfBooks = parseInt($('.count-book .num-quantity').val()),
		designCost = parseInt($('.dt-inputs .val-print-price').attr('data-price'));
		
	if(discount === 0) {
		$('.itog').hide();
	}

	for(i = 0; i < $('.box-konstruktr tr').length-1; i++) {
		if ($('.box-konstruktr tr').eq(i).find('td').hasClass('price-field') && !$('.box-konstruktr tr').eq(i).find('td').hasClass('fc-points')) {
			var a = $('.box-konstruktr tr').eq(i).find('td.price-field');
			if(a.hasClass('count-pager')) {
				masPrice[j] = parseInt(a.find('.val-print-price').val()*a.find('.num-quantity').attr('data-price'));
			} 

			else if (a.hasClass('count-active')) {
				masPrice[j] = parseInt(a.find('.active .val-print-price').attr('data-price'));
			} 

			else if (a.hasClass('count-checked')) {
				masPrice[j]	= parseInt(a.find('.val-print-price:checked').attr('data-price'));
			} 

			else if (a.hasClass('count-input')) {
				a.find('.val-print-price').each(function() {
					masPrice[j] =  parseInt($(this).attr('data-price'));
				});
			} 

			else if (a.hasClass('count-personal')) {
				masPrice[j] =  parseInt(a.find('.active').attr('data-price'));
			}

			else if (a.hasClass('count-each')) {
				a.find('.val-print-price').each(function() {
					countPrice = parseInt($(this).attr('data-price')) + countPrice;
				});
				masPrice[j] = countPrice;
			}
			j++;
		}

		else if($('.box-konstruktr tr').eq(i).find('td').hasClass('price-field') && $('.box-konstruktr tr').eq(i).find('td').hasClass('fc-points')) {
			var b = $('.box-konstruktr tr').eq(i).find('td.price-field');
			if (b.hasClass('count-input')) {
				sumMin = parseInt(b.find('.val-print-price').attr('data-price')) + sumMin;
			}

			else if(b.hasClass('count-active')) {
				sumMin = parseInt(b.find('.active .val-print-price').attr('data-price')) + sumMin;
			} 

			else if (b.hasClass('count-checked')) {
				sumMin = parseInt(b.find('.val-print-price:checked').attr('data-price')) + sumMin;
			}
			else if (b.hasClass('count-personal')) {
				sumMin =  parseInt(b.find('.val-print-price').attr('data-price')) + sumMin;
			}

			else if(b.hasClass('count-pager')) {   
				var minibookQuantity = parseInt(b.find('.val-print-price').val());  
				var minibookPrice = parseInt(b.find('.num-quantity').attr('data-price'));
			}
		}
	}

	if (isNaN(numberOfBooks)) numberOfBooks = 1;
	if (isNaN(designCost,minibookQuantity)) designCost =0;
	if (isNaN(minibookQuantity)) minibookQuantity =0;
	if (isNaN(minibookPrice)) minibookPrice =0;

	sumMin = (sumMin + minibookPrice) * minibookQuantity;

	for(j = 0; j < masPrice.length; j++) {
		sum = sum + masPrice[j];
	}

	var summa = ((base + sum)*numberOfBooks + sumMin) + designCost;

	$('.add-info_minibook span').html(sumMin + ' <span class="rub">P</span>');
	$('.price-table .itog strike').html(summa + ' <span class="rub">P</span>');
	$('.price-table .sum span').html(Math.ceil(summa-(summa*discount)) + ' <span class="rub">P</span>');
}

/*********************START*****************************/
$(function(){

	$(window).resize(function(){
		bigSlider();
	});

	$(window).load(function(){
		if ('ontouchstart' in document) {
		    $('body').removeClass('no-touch');
		}
		if ($('.top-marker').css('opacity') <= 0) $('.top-marker').addClass('hidden');
			else {$('.top-marker').removeClass('hidden');}
		bigSlider();
		hideme();
		preloader();
		delivery();
	});

	$(window).scroll( function(){
		someParallax();
		fixedMenu();
		hideme();

		$('.top-marker').css({
			'opacity' : ($(window).scrollTop()-$(window).height()/4)/100
		});

		if ($('.top-marker').css('opacity') <= 0) $('.top-marker').addClass('hidden');
			else {$('.top-marker').removeClass('hidden');}

		if($(window).scrollTop() + $('footer').height() >= $(document).height() - window.innerHeight) {
			$('.top-marker').css('bottom', -$(document).height()+window.innerHeight+$(window).scrollTop()+ $('footer').height())
		} else {
			$('.top-marker').css('bottom','0')
		}
	});

	$('body').on('click', 'a.help-block, .link-marker', function (e){
		e.preventDefault();
        var el = $(this).attr('href');
        var elTop =  $(el).offset().top-200;
        $('body,html').animate({scrollTop: elTop}, 2000); 
    });

	$('.req-modal').on('click', function () {
    		$(this).parents('.bg-konstr').find('.req-modalWin').kamod();
    	});

	$('.hideme, .hideme-list2:not(:nth-of-type(-n+2)), .hideme-list3:not(:nth-of-type(-n+3)) ').css('opacity','0');

	

	$('.big-slider .bxslider').bxSlider({
		startSlide: 0,
		pagerCustom: '.bx-pager',
		infiniteLoopL: true,
		touchEnabled: true,
		useCSS: false,
		auto: true,
		pause: 7000
	});

	$('.slider-book .bxslider').bxSlider({
		startSlide: 0,
		pagerCustom: '.bx-pager',
		infiniteLoopL: true,
		touchEnabled: true,
		useCSS: false,
		auto: false
	});

	$('.big-slider .bx-pager a').on('click',function(){
		$('.bxslider .box-text p, .bxslider .box-text a').slideUp(0);
		var numb = $(this).attr('data-slide-index');
		$('.big-slider .bxslider .box-text a').fadeIn(1000);
		$('.big-slider .bxslider').children('[data-slide-count=' + numb + ']').find('a,p').slideDown(1500);
	});

	$('.user-size').parent().find('input').addClass('hidden');
	$('.book-size span').on('click', function() {
		if($(this).hasClass('user-size')) {
			$(this).addClass('active');
			$(this).parent().find('.i-user-size').removeClass('hidden').inputmask("9{1,3} x 9{1,3}");
		}
		else {
			$(this).parent().find('input').addClass('hidden');
		}
	});

	$('.field-users-phone input').on('focus', function() {
		$(this).inputmask("+7(9{3})9{3}-9{3}-9{2}");
	});

	$('.check-point').on('click', function() {
		checkAddAttr();
	});

	$('.block-order .checkboxes input').on('change', function() {
		delivery();
	});

	arrow();
	activity();
	bookMatelial();
	bookMatelial();
	coloredIt();
	personalization();
	valide();
	konstructorSlider();
	modalInput();
	modalDesignTurns();
	parenquantity();
	comboMatirial();
	materialCorob();
	checkAddAttr();
	nextConstructorPage();
	CalculateIt();
	dvdDisable();

	$('.box-konstruktr *, .box-modal, .block-designTurns *, .farz').on('click',function() {
		CalculateIt();
		$('.fill').on('click', function() {
			$(this).parents('td').find('.help-block').remove();
		});
	});

	if($('*').hasClass('zoom-foto')) {
		$(".zoom-foto").imagezoomsl({
	    	zoomrange: [3, 3],
	    	magnifiersize : [400, 400]
		});
	}

		$('.js-owlSlider .owl-carousel, .js-owlSlider .owl-carousel3-farz').owlCarousel({
     itemsCustom : [
        [0, 1],
        [650, 3],
        [1000, 3]
      ],
       navigation : true
	});
	
	$('.js-owlSlider .owl-carousel-farz').owlCarousel({
	     itemsCustom : [
	        [0, 1],
	        [650, 4],
	        [1000, 4]
	      ],
	       navigation : true
	});

	$('.js-owlSlider .owl-carousel5-farz').owlCarousel({
     itemsCustom : [
        [0, 1],
        [650, 1],
        [1000, 1]
      ],
       navigation : true
	});
});