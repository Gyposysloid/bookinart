var syncCB = $('.syncCB');
var syncCM = $('.syncCM');

syncCB.owlCarousel({
  singleItem : true,
  slideSpeed : 1000,
  navigation: true,
  pagination:false,
  afterAction : syncPosition,
  responsiveRefreshRate : 200,
});

syncCM.owlCarousel({
  items : 3,
  pagination:false,
  responsiveRefreshRate : 100,
  afterInit : function(elc){
    elc.find(".owl-item").eq(0).addClass("synced");
  }
});

function syncPosition(elc){
  var current = this.currentItem;
  $(".syncCM")
    .find(".owl-item")
    .removeClass("synced")
    .eq(current)
    .addClass("synced")
  if($(".syncCM").data("owlCarousel") !== undefined){
    center(current)
  }
}

$(".syncCM").on("click", ".owl-item", function(e){
  e.preventDefault();
  var number = $(this).data("owlItem");
  syncCB.trigger("owl.goTo",number);
});

function center(number){
  var syncCMvisible = syncCM.data("owlCarousel").owl.visibleItems;
  var num = number;
  var found = false;
  for(var i in syncCMvisible){
    if(num === syncCMvisible[i]){
      var found = true;
    }
  }

  if(found===false){
    if(num>syncCMvisible[syncCMvisible.length-1]){
      syncCM.trigger("owl.goTo", num - syncCMvisible.length+2)
    }else{
      if(num - 1 === -1){
        num = 0;
      }
      syncCM.trigger("owl.goTo", num);
    }
  } else if(num === syncCMvisible[syncCMvisible.length-1]){
    syncCM.trigger("owl.goTo", syncCMvisible[1])
  } else if(num === syncCMvisible[0]){
    syncCM.trigger("owl.goTo", num-1)
  }
  
}